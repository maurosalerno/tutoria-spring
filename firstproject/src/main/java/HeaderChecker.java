package com.testing.springboot;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.testing.service.ValidateHeader;


import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class HeaderChecker {

    static Logger logger = Logger.getLogger(HeaderChecker.class);
    //Tell spring to inject service in here
    @Autowired
    private ValidateHeader service;

    @RequestMapping(value = "/checkhead", method = GET)
    public String checking(@RequestAttribute("validationMessage") String validationMessage){
        return service.retrieveWelcomeMessage(validationMessage);
    }


        //...
    @ExceptionHandler(Exception.class)
    public void handleException() {
        logger.info("We handled the exception!");
    }

}
