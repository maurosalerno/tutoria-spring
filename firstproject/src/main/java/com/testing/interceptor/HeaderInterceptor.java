package com.testing.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class HeaderInterceptor extends HandlerInterceptorAdapter {
    static Logger logger = Logger.getLogger(HeaderInterceptor.class);
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        String testHeader = request.getHeader("Test-Header");
        Enumeration<String> headers = request.getHeaderNames();
        String header, headerName;
        //throw new java.lang.RuntimeException("Welp this went badly");
        while(headers.hasMoreElements()){
            headerName = headers.nextElement();
            header = request.getHeader(headerName);
            logger.info(header+headerName);
        }
        if(testHeader == null){
            logger.info("Test-Header not in request!");
            request.setAttribute("validationMessage", "Test-Header was not found in the request!\n");
        }
        else {
            Pattern pattern = Pattern.compile("[a-zA-Z ]+");
            Matcher matcher = pattern.matcher(testHeader);
            if(matcher.matches()){
                logger.info("Test-Header is a string with only letters! This is the value: " + testHeader);
                request.setAttribute("validationMessage",
                        "Test-Header is a string with only letters! This is the value: " + testHeader + "\n");
            }
            else{
                logger.info("Test-Header has characters that aren't letters! This is the value: " + testHeader);
                request.setAttribute("validationMessage",
                        "Test-Header has characters that aren't letters! This is the value: " + testHeader + "\n");
            }
        }

        return true;
    }
}