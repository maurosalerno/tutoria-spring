package com.testing.service;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.xml.validation.Validator;

@Component
public class ValidateHeader {
    static Logger logger = Logger.getLogger(ValidateHeader.class);
    public String retrieveWelcomeMessage(String message){
        logger.info("The service retrieveWelcomeMessage was called!");
        return "Good morning! " + message;
    }

}