package com.testing.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

//Set spring to manage this "bean", and create an instance. Can use @cComponent or @Service
@Component
public class WelcomeService{
    public String retrieveWelcomeMessage(){
        return "Good morning! Default welcome message, i guess.";
    }
}