package com.testing.config;

import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.testing.interceptor.HeaderInterceptor;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HeaderCheckConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new HeaderInterceptor());
    }

}
